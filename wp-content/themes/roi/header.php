<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
  <!--[if lt IE 9]>
  <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
  <![endif]-->
  <script>(function(){document.documentElement.className='js'})();</script>
  <?php wp_head(); ?>

  <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/foundation.css" media="screen" />
  <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/font-awesome.css" media="screen" />
  <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/style.css" media="screen" />
  <script src="<?php bloginfo('template_directory'); ?>/js/vendor/modernizr.js"></script>
</head>

<body <?php body_class(); ?>>

  <header>
    <div class="row">
      <div class="medium-4 large-3 columns">
        <h1>
          <a href="<?php echo get_bloginfo('url') ?>"><img src="<?php echo get_bloginfo('template_directory') ?>/img/logo.png" /></a>
        </h1>
      </div>
      <div class="medium-8 large-8 large-offset-1 columns">
        <h5 class="subheader">Bereken uw Return On Investment</h5>
      </div>
    </div>
  </header>

  <div id="pjax-container">