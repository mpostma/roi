</div>

<footer>
  <div class="row">
    <div class="small-12 columns">
      &copy; Copyright <?php print date('Y') ?> - <?php echo get_bloginfo('name') ?>
    </div>
  </div>
</footer>

<img src="<?php bloginfo('template_directory'); ?>/img/gears.svg" id="loader" width="32" height="32" />

<script src="<?php bloginfo('template_directory'); ?>/js/vendor/jquery.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/foundation.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/lib/jquery.sticky.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/lib/jquery.pjax.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/calculation.js"></script>

<?php wp_footer() ?>

</body>
</html>