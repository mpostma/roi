$ = jQuery.noConflict();

//
// Serialize form data
//
$(document).on('keyup', 'form#data',(function(){
  var form =   $(this)
  var data =   form.serializeArray()

  //
  // VARIABLES
  //

    // Set each submitted form value as variable
    var turnover      = $(form).find('input[name=turnover]').val()
    var margin        = $(form).find('select[name=margin]').val()
    var order_value   = $(form).find('input[name=order_value]').val()
    var conversion    = $(form).find('input[name=conversion]').val()
    var budget        = $(form).find('input[name=budget]').val()

    // Make calculations based on form values
    var required      = Math.round( turnover / order_value )
    var visitors      = Math.round( 100 / conversion * required)
    var adwords       = 35 / 100 * visitors; // 35%
    var links         = 10 / 100 * visitors; // 10%
    var payed_visitors= adwords + links // 45%
    var budget_euro   = turnover / 100 * budget
    var cpc           = budget_euro / payed_visitors

  //
  // POPULATE CALCULATION
  //

    $('#required').find('.value').text(number_format(required))
    $('#visitors').find('.value').text(number_format(visitors))
    $('#adwords').find('.value').text(number_format(adwords))
    $('#links').find('.value').text(number_format(links))
    $('.payed_visitors').text(number_format(payed_visitors))
    $('.budget').text(number_format(budget_euro))
    $('.budget-percentage').text(budget)
    $('.cpc').text( decimal_format(cpc, 2) );

  //
  // OTHER BEHAVIOUR
  //

    // CPC
    if(cpc == 1 || cpc > 1) {
      $('#grade').addClass('valid').removeClass('invalid')
      $('#grade .fa').addClass('fa-check').removeClass('fa-warning')
    } else {
      $('#grade').addClass('invalid').removeClass('valid')
      $('#grade .fa').addClass('fa-warning').removeClass('fa-checkmark')
    }

    hide_loader()

}))

$(document).on('keydown', 'form#data',(function(){
  show_loader()
}))

//
// Bindings
//

  // Document ready
  $(document).ready(function(){
    $('form#data').trigger('keyup')

    $(document).foundation()
  })

  // Pjax
  $(document).on('pjax:complete', function() {
    $('form#data').trigger('keyup')
    hide_loader()
  })

  $(document).on('pjax:send', function() {
    $('form#data').trigger('keyup')
    show_loader()
  })

  // Form bindings
  $(document).on('change', 'form#data select[name=margin]', (function(event){
    event.preventDefault()

    var value = $(this).val()
    var input = $('form#data input[name=budget]')

    if(value == 0) {
      input.val(2.5)
    } else if(value == 15) {
      input.val(7.5)
    }

    $('form#data').trigger('keyup')
  }))

  $(document).on('submit', (function(event){
    event.preventDefault()
  }))

  $(document).pjax('a', '#pjax-container')

//
// Functions
//

  // Loader functions
  function show_loader() {
    $('#loader').stop(true,true).fadeIn(100)
  }

  function hide_loader() {
    $('#loader').delay(350).fadeOut(100)
  }

  // Default number formatting
  function number_format (number) {
    return decimal_format(number, 0);
  }

  // Convert decimal to human formatted number
  function decimal_format(number, decimals) {
    var dec_point = ',', thousands_sep = '.';
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
      prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
      sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
      dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
      s = '',
      toFixedFix = function (n, prec) {
          var k = Math.pow(10, prec);
          return '' + Math.round(n * k) / k;
      };
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
      s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
      s[1] = s[1] || '';
    }
    if ((s[1] || '').length < 1) {
      return s[0];
    }
    return s.join(dec);
  }

