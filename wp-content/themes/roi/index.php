<?php if (!isset($_SERVER['HTTP_X_PJAX'])) get_header(); ?>

<div id="content">

  <div class="row">
    <?php include('layout/form.php') ?>

    <div class="medium-8 columns" id="results">
      <div>
        <div class="row">
          <h4 class="small-12 columns">Rapport</h4>
          <div id="required" class="small-6 columns">
            <h6>
              Benodigde bestellingen<br />
              <small>Omzet / bestelwaarde</small>
            </h6>
            <h3 class="value"></h3>
          </div>

          <div class="small-6 columns">
            <h6>
              Marketing budget<br />
              <small><span class="budget-percentage"></span>% van de omzet</small>
            </h6>
            <h3>&euro; <span class="budget"></span></h3>
          </div>
        </div>

        <hr/>

        <div class="row">

          <h4 class="small-12 columns">Bezoekers</h4>

          <div id="visitors" class="small-6 columns">
            <h6>
              Totaal bezoekersaantal<br />
              <small>Op basis van omzet en bestelwaarde</small>
            </h6>
            <h3 class="value">...</h3>
          </div>

          <div id="adwords" class="small-6 columns">
            <h6>
              Via Adwords<br />
              <small>35% via advertenties op Google</small>
            </h6>
            <h3 class="value">...</h3>
          </div>

          <div id="links" class="small-6 columns">
            <h6>
              Verwijzende links<br />
              <small>10% via betaalde links</small>
            </h6>
            <h3 class="value">...</h3>
          </div>

          <div class="small-6 columns">
            <h6>
              Totaal betaald bezoek<br />
              <small>45% via links en advertenties</small>
            </h6>
            <h3 class="payed_visitors">...</h3>
          </div>
        </div>

        <hr/>

        <div class="row">

          <div id="grade" class=" small-12 columns">
            <div class="panel radius">
              <div class="fa icon fa-2x"></div>
              <div class="row collapse">
                <div class="small-8 columns">
                  <table>
                    <tbody>
                      <tr>
                        <th>Gemiddelde CPC is</th>
                        <td>&euro; <span class="budget"></span> / <span class="payed_visitors"></span> bezoekers</td>
                      </tr>
                      <tr>
                        <th>
                          Budget per bezoeker</br>
                          <small>Minimaal &euro;1.00 per click</small>
                        </th>
                        <th><h2>€ <span class="cpc">1,67</span></h2></th>
                      </tr>
                    </tbody>
                  </table>

                </div>

                <div class="small-4 columns">
                  <span class="valid-text">Uw budget is toereikend!</span>
                  <span class="invalid-text">LET OP: Uw budget is niet toereikend!</span>
                </div>

              </div>
            </div>
          </div>

        </div>

      </div>
    </div>
  </div>

</div>

<?php if (!isset($_SERVER['HTTP_X_PJAX'])) get_footer() ?>
