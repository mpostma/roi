<form class="medium-4 columns" id="data">

  <h4 class="small-12 columns">Uw gegevens</h3>

    <div id="turnover" class="medium-12 columns">
      <div class="row collapse">
        <label>Omzet per maand</label>
        <div class="small-3 columns">
          <span class="prefix">&euro;</span>
        </div>
        <div class="small-9 columns">
          <input id="turnover" type="text" name="turnover" value="100000" />
        </div>

      </div>
    </div>

    <div class="medium-12 columns">
      <label>Marge</label>
      <select name="margin">
        <option value="15">&gt; 15%</option>
        <option value="0">&lt; 15%</option>
      </select>
    </div>

    <div class="medium-12 columns">
      <div class="row collapse">
        <label>Gemiddelde bestelwaarde</label>
        <div class="small-3 columns">
          <span class="prefix">&euro;</span>
        </div>
        <div class="small-9 columns">
          <input type="text" name="order_value" value="100" />
        </div>
      </div>
    </div>

    <div class="medium-6 columns">
      <div class="row collapse">
        <label>Marketingbudget</label>
        <div class="small-7 columns">
          <input type="number" value="7.5" name="budget" />
        </div>
        <div class="small-5 columns">
          <span class="postfix">%</span>
        </div>
      </div>
    </div>

    <div class="medium-6 right columns">
      <div class="row collapse">
        <label>Conversie</label>
        <div class="small-7 columns">
          <input type="text" value="10" name="conversion" />
        </div>
        <div class="small-5 columns">
          <span class="postfix">%</span>
        </div>
      </div>
    </div>

</form>